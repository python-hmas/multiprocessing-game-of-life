# --------------------------------------------------------------------------------------------------------------------------------
#   Humberto Alvarez | 2020
# --------------------------------------------------------------------------------------------------------------------------------
#
#   Implementation of cellular simulation based on the rules of John Conway's "Game of Life", with multiprocessing support.
#   This program takes as input an initial matrix containing "cells", and will simulate the next 100 generations
# --------------------------------------------------------------------------------------------------------------------------------

import sys, getopt
import multiprocessing 

def main(argv):
    print("Game of Life")

    inputfile = ''
    outputfile = ''
    threadNum = 1
    try:
       opts, args = getopt.getopt(argv,"i:o:t:",["ifile=","ofile=","threads="])
    except getopt.GetoptError:
        print('file.py -i <path_to_input_file> -o <path_to_output_file> -t <number_of_threads>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-t", "--threads"):
            threadNum = int(arg)
    
    print("Reading input from file " + inputfile)

    with open(inputfile,"rt") as infile:
        matrix = [list(line.strip()) for line in infile.readlines()]

    rowsPerThread = splitTasks(len(matrix) , threadNum) #   array holding ranges for the assigned rows for each process
    manager = multiprocessing.Manager()
    q = manager.Queue() #   queue passed as an argument for each process call. next generation for cells in a row are enqueued into queue.

    print("Simulating...")
    for i in range(100):
        print("processing generation...", i)
        processes = []
        for j in range(1, threadNum + 1):   #   each process is in charge of a number of rows in range dependeing on the number of threads pass down as command line argument
            p = multiprocessing.Process(target=threadTask, args=(rowsPerThread[j-1], rowsPerThread[j], matrix, q))
            p.start()
            processes.append(p)
        
        for p in processes:
            p.join()

        unsorted = [q.get() for _ in range(rowsPerThread[-1])]
        matrix = [val[1] for val in sorted(unsorted)]  


    with open(outputfile, 'w') as f:
       for i in range(len(matrix)):
            string = matrix[i]
            f.write("".join(str(val) for val in string) + "\n")
    
    print("Simulation complete. Final result stored in the output file " + outputfile)

def updateRow(i, matrix):   #   returns the next generation for the cells in a single row of the matrix
    cols = len(matrix[0])
    rows = len(matrix)
    nextGen = [0 for a in range(cols)]
    for x in range(cols):
        neighbors = []
        neighbors.append(matrix[checkCorner(i-1, rows)][checkCorner(x-1, cols)])
        neighbors.append(matrix[checkCorner(i-1, rows)][checkCorner(x, cols)])
        neighbors.append(matrix[checkCorner(i-1, rows)][checkCorner(x+1, cols)])
        neighbors.append(matrix[checkCorner(i, rows)][checkCorner(x-1, cols)])
        neighbors.append(matrix[checkCorner(i, rows)][checkCorner(x+1, cols)])
        neighbors.append(matrix[checkCorner(i+1, rows)][checkCorner(x-1, cols)])
        neighbors.append(matrix[checkCorner(i+1, rows)][checkCorner(x, cols)])
        neighbors.append(matrix[checkCorner(i+1, rows)][checkCorner(x+1, cols)])

        deadCount = 0
        aliveCount = 0
            
        for n in neighbors:
            if n == '.':
               deadCount+=1
            elif n == 'O':
                aliveCount += 1

        if matrix[i][x] == 'O':
            if aliveCount >= 2 and aliveCount <= 4:
                nextGen[x] = 'O'
            else:
                nextGen[x] = '.'
        
        elif matrix[i][x] == '.':
            if aliveCount > 0 and aliveCount % 2 == 0:
                nextGen[x] = 'O'
            else:
               nextGen[x] = '.'
            
    return nextGen

def checkCorner(index, max):    #   function that prevents from going out of range in a matrix
    if index == max:
        return 0
    elif index == -1:
        return max -1
    else:
        return index

def threadTask(start, end, matrix, q):  #   Function called by each process
    for i in range(start, end):
        q.put((i, updateRow(i, matrix)))

def splitTasks(n, d):   #   function that returns an array of indexes of the rows that will be assigned for each process
    tasks = [ 0 ]
    if(n < d):  
        print("excessive number of tasks")
        exit(2)
   
    elif (n % d == 0): 
        for i in range(d): 
            tasks.append(tasks[-1] + n//d)
    else: 
        remainder = d - (n % d) 
        prod = n//d 
        for i in range(d): 
            if(i >= remainder): 
                tasks.append(tasks[-1] + prod + 1)
            else:
                tasks.append(tasks[-1] + prod)

    return tasks
    
if __name__ == "__main__":
   main(sys.argv[1:])
