# Multiprocessing Game of Life

Implementation of Conway's game of life using multiprocessing in Python.
User is able to select number of threads in command line argument.

dead cells are represented by (.)

live cells are represented by (O)

The number of rows in the cell matrix must be less than or equal to the number of processes

Program is run through command line argument of the form:
```
file.py -i <path_to_input_file> -o <path_to_output_file> -t <number_of_processes>
```

The first 100 generations of the Game of Life will be simulated, and the final generation will be saved under the path to the output file.